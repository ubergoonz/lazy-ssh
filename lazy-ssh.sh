#!/bin/bash
# my hack to alias ssh to all host just by typing hostname or user@hostname
# 1. install to any directory of your choice recommended to be ~/bin/lazy-ssh
# 2. add to your PATH, 
#

B=$(dirname $0)
LAZY_DIR=$(cd $B ; pwd)
H=$(basename $0)
if [ -h $0 ]; then
    echo "## INFO sshing to $H"
    ssh $H
else
    if [ $# -eq 0 ]; then
        echo "No hostname specified. prompting for hostname ..."
        echo "You may use hostname or user@hostname"
        read -rep "Enter the [hostname/user@hostname] ssh to: " sshto
    fi
    if [ $# -gt 0 ]; then
        sshto=$1
    fi
    echo "## NOTIFICATION Creating a softlink of $sshto for future use"
    cd $LAZY_DIR
    ln -s lazy-ssh.sh $sshto &>/dev/null
    echo "## INFO sshing to $sshto"
    ssh $sshto
    echo "You can run the shortcut $sshto to autmatically ssh into $sshto !"
fi
