#!/bin/bash
# an install script to install lazy-ssh.sh
: ${LAZY_HOME:=~/bin/lazy-ssh}
if [ -d $LAZY_HOME ]; then
    echo "## INFO Detected a copy of ${LAZY_HOME} .. backing up to ${LAZY_HOME}.$$"
    mv ${LAZY_HOME} ${LAZY_HOME}.backup.$$
fi
mkdir -p ${LAZY_HOME}
cp lazy-ssh.sh ${LAZY_HOME}
cd ${LAZY_HOME}
for known_host in `cat ~/.ssh/known_hosts | awk -F, '{print $1}'`
do
    echo "## INFO Adding softlink shortcut to SSH known_host $known_host..."
    ln -s lazy-ssh.sh $known_host &>/dev/null
    ls -al $known_host
    echo " ------- "
done
case $SHELL in
    *zsh)   echo "You can add the following line into your ~/.zshrc" ;;
    *bash)  echo "You can add the following line into your ~/.bashrc" ;;
    *)      echo "You can add the following line into your startup script" ;; 
esac
echo ""
echo "export PATH=\$PATH:${LAZY_HOME}"
echo ""
