# lazy-ssh
For all the lazy people that like to ssh to many hosts

my hack to alias ssh to all host just by typing hostname or user@hostname
1. install to any directory of your choice recommended to be `~/bin/lazy-ssh`
2. add to your PATH   
  - BASH/KSH/ZSH --> `export PATH=$PATH:~bin/lazy-ssh`

## Using it ...
### First run
```
$ lazy-ssh <hostname>
$ lazy-ssh <user>@<hostname>
```
a softlink of `<hostname>` to `lazy-ssh` is created 

### Subsequent runs...
```
$  <hostname>
$  <user>@<hostname>
```

Using the install script `inst-lazy-ssh.sh`
 - The install script `inst-lazy-ssh.sh` will install the `lazy-ssh.sh` script into `~/bin/lazy-ssh`.
 - If you have existing `~/.ssh/known_hosts` file, it will create the `<hostname>` softlinks into `~/bin/lazy-ssh/`.
